<!DOCTYPE html>
<html lang="fr">

  <head>

      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>Blog</title>

      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  </head>

  <body>
    <?php
    try {

      $bdd = new PDO('mysql:host=localhost;dbname=Monblog','root','');

    } catch (Exception $e) {

      die('Erreur : ' . $e->getMessage());
    }

    $recup = $bdd->query('SELECT * FROM blog');

    while ($donnees = $recup->fetch()) {

    }
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                    <!-- LE NOMBRE TOTAL DE POST -->
                <h1 class="page-header">
                    VOUS AVEZ CREER
                    <?php
                    $recup = $bdd->query('SELECT COUNT(*) AS nb_total_post FROM blog');
                    echo $donnees($_POST['nb_total_post']);
                    ?>
                    POSTS
                </h1>

                <!-- Le post -->
                <h2>
                    <!-- LE TITRE DU BLOG-->
                    <a href="#"><?php echo $donnees($_POST['title']); ?></a>
                </h2>
                    <!-- LE NOM DE L'AUTEUR -->
                <p class="lead">
                    par  <?php echo $donnees($_POST['auteur']); ?>
                </p>
                    <!-- LA DATE ET L'HEURE -->
                <p><span class="glyphicon glyphicon-time"></span> Posté le <?php echo $donnees($_POST['date_creation']); ?></p>
                <hr>
                    <!-- LE CONTENU DU TEXTE -->
                <p> <?php echo $donnees($_POST['content']); ?></p>
                  <a class="btn btn-primary" href="#">Lire le commentaire<span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>

            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>

  </body>
</html>
