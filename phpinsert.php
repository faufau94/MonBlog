<?php

try {

  $bdd = new PDO('mysql:host=localhost;dbname=Monblog','root','');

} catch (Exception $e) {

  die('Erreur : ' . $e->getMessage());
}

if (isset($_POST['auteur']) AND isset($_POST['title']) AND isset($_POST['content']))
{
  if (!empty($_POST['auteur']) AND !empty($_POST['title']) AND !empty($_POST['content']))
  {

    $auteur = htmlspecialchars($_POST['auteur']);
    $titre = htmlspecialchars($_POST['title']);
    $content = sha1($_POST['content']);

    try {

      $insert = $bdd->prepare('INSERT INTO blog(Auteur,Titre,Contenu,date_creation) VALUES(?,?,?,NOW())');
      $insert->execute(array($auteur, $titre, $content));

    } catch (Exception $e) {

      die('Erreur : ' . $e->getMessage());

    }

    header('Location: blog.php');
  }

}

 ?>
